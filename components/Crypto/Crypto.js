import React from 'react'
import axios from 'axios'
import CountUp from 'react-countup'
import styles from './Crypto.module.scss'

export default class Crypto extends React.Component {
  constructor (props) {
    super(props)
    this.state = { coins: ['bitcoin', 'ethereum', 'cardano'], data: [], dataPrev: [] }
  }

  async componentDidMount () {
    const interval = Math.ceil(60 / (Math.floor(100 / this.state.coins.length))) * 1000 * 4
    await this.getAllCoins()
    setInterval(() => this.getAllCoins(), 30000)
  }

  async getAllCoins () {
    let arr = []

    for (let i = 0; i < this.state.coins.length; i++) {
      const currCoin = this.state.coins[i]
      const data = await this.getCoin(currCoin)
      if (data) {
        arr.push(data)
      } else {
        arr.push(this.state.dataPrev[i])
      }
    }
    this.setState({ coins: this.state.coins, data: arr, dataPrev: this.state.data })
  }

  async getCoin (name) {
    return await axios.get(`https://api.coingecko.com/api/v3/coins/${name}`).then((res) => {
      return res.data
    }).catch((err) => {
      console.log(err)
    })
  }

  isInt = n => n % 1 === 0

  getDecimal = number => (this.isInt(number) ? 0 : 2)

  render () {
    return (<div className={styles.cryptoScroller}>
      <div className={styles.scroll}>
      {this.state.data.map((coin, index) => (
        <span key={index} className={styles.coin}>
          <img src={coin.image.thumb}/>
          <span>{coin.name}</span>
          {/*<span>{coin.market_data.last_updated}</span>*/}
          <CountUp prefix={'$'}
                   decimals={this.getDecimal(coin.market_data.current_price.usd)}
                   start={this.state.dataPrev[index] ? this.state.dataPrev[index].market_data.current_price.usd : 0}
                   end={coin.market_data.current_price.usd}/>
          <CountUp style=
                     {coin.market_data.price_change_percentage_24h_in_currency.usd >= 0
                       ? { color: 'green' } : { color: 'red' }}
                   suffix={'%'}
                   decimals={this.getDecimal(parseFloat(coin.market_data.price_change_percentage_24h_in_currency.usd.toFixed(2)))}
                   start={this.state.dataPrev[index] ? parseFloat(this.state.dataPrev[index].market_data.price_change_percentage_24h_in_currency.usd.toFixed(2)) : 0}
                   end={parseFloat(coin.market_data.price_change_percentage_24h_in_currency.usd.toFixed(2))}/>
        </span>
      ))}
      </div></div>)
  }
}
