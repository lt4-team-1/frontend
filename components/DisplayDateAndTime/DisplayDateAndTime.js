import React, { useEffect, useState } from 'react'
import styles from './DisplayDateAndTime.module.scss'

const DisplayDateAndTime = () => {

  // creating arrays for Days and Months
  const daysArray = ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag']
  const monthArray = ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli',
    'Augustus', 'September', 'Oktober', 'November', 'December']
  const currentDate = new Date().getDate()

  // useState stuff
  const [day, setDay] = useState(daysArray[new Date().getDay()])
  const [date, setDate] = useState(currentDate)
  const [month, setMonth] = useState(monthArray[new Date().getMonth()])
  const [time, setTime] = useState(new Date().toLocaleTimeString())

  // useEffect every second for displaying the time in the component
  useEffect(() => {
    const interval = setInterval(() => {
      setDay(daysArray[new Date().getDay()])
      setDate(new Date().getDate())
      setMonth(monthArray[new Date().getMonth()])
      setTime(new Date().toLocaleTimeString())
    }, 1000)
    return () => clearInterval(interval)
  }, [])

  // return some html
  return (
    <div className={styles.itembar}>
      <div>
        {day} {date} {month}
      </div>
      <div>
        {time}
      </div>
    </div>
  )
}

export default DisplayDateAndTime
