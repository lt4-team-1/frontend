import StoryPointBlock from './StoryPointBlock/StoryPointBlock'
import styles from './StoryPoints.module.scss'
import { useState } from 'react'

export default function StoryPoints () {
  const [StoryPoints] = useState([
    { name: 'Storypoints in backlog', amount: 60 },
    { name: 'Storypoints to do', amount: 4.2 },
    { name: 'Velocity', amount: 6.5 }
  ])
  return (<div className={styles.StoryPoints}>
    {StoryPoints.map((data, index) => (
      <StoryPointBlock key={index} label={data.name} storypoints={data.amount}/>
    ))}
  </div>)
}
