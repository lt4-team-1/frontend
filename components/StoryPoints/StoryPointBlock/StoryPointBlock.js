import styles from './StoryPointBlock.module.scss'

const StoryPointBlock = ({ label, storypoints }) => {
  return (
    <div className={styles.StorypointBlock}>
      <span>{label}</span>
      <span>{storypoints}</span>
    </div>
  )
}

export default StoryPointBlock
