import React, { useEffect, useState } from 'react'
import styles from './DaysLeft.module.scss'

const DaysLeft = () => {

  const [daysLeft, setDaysLeft] = useState('')
  const [currentSprint, setCurrentSprint] = useState('')

  useEffect(() => {
    getDaysLeft()
    getCurrentSprint()
  }, [])

  const getDaysLeft = async () => {
    const response = await fetch(`http://localhost:8080/sprints/daysLeft`)
    const data = await response.json()
    setDaysLeft(data)
  }

  const getCurrentSprint = async () => {
    const response = await fetch(`http://localhost:8080/sprints/current`)
    const data = await response.json()
    setCurrentSprint(data.name)
  }

  return (
    <div className={styles.currentSprint}>
      {currentSprint}
      <br/>
      Aantal dagen resterend: {daysLeft}
    </div>
  )
}

export default DaysLeft
