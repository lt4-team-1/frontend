import React, { useEffect, useState } from 'react'
import { Doughnut } from 'react-chartjs-2'
import styles from './PieChart.module.scss'

function PieChart () {

  const [data, setData] = useState({})

  useEffect(() => {
    setData({
      labels: ['To do', 'Completed'],
      datasets: [
        {
          label: '',
          backgroundColor: [
            'rgb(124,48,64)',
            'rgb(23,65,93)',
          ],
          hoverBackgroundColor: [
            'rgb(246,28,74)',
            'rgb(4,122,203)',
          ],
          data: [5, 10]
        }
      ]
    })
  }, [])

  return (
    <div className={styles.PieChart}>
      <Doughnut
        data={data}
        options={{
          title: {
            display: true,
            text: 'StoryPoints Doughnut',
            fontSize: 20
          },
          legend: {
            display: true,
            position: 'right'
          }
        }}
      />
    </div>
  )
}

export default PieChart
