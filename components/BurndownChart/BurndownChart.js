import React from "react";
import styles from "./BurndownChart.module.scss";
import { Line } from "react-chartjs-2";

const BurnDownChart = () => {
  const showBurnDownChart = (burndownData) => {
    const totalPointsInSprint = burndownData[0];
    //Give the sprint a length
    const sprintLength = 10;
    const pointsIdealDay = totalPointsInSprint / (sprintLength - 1);
    const idealLine = [];
    const sprintDays = [];

    for (let i = 0; i < sprintLength; i++) {
      sprintDays.push("Dag " + (i + 1));
      if (i == sprintLength - 1) {
        idealLine.push(0);
      } else {
        idealLine.push(totalPointsInSprint - pointsIdealDay * i);
      }
    }

    return (
      <Line
        data={{
          labels: sprintDays,
          datasets: [
            {
              data: burndownData,
              label: "Burndown",
              borderColor: "red",
              fill: false,
            },
            {
              data: idealLine,
              label: "Ideaal",
              borderColor: "white",
              lineTension: 0,
              borderDash: [5, 5],
              fill: false,
            },
          ]
        }}
        options={{
          interaction: {
            mode: 'index'
          }
        }}
      />
    );
  };

  return (
    <div className={styles.burndown_chart}>
      {showBurnDownChart(
        //   1    2    3    4    5    6    7    8    9   10
        [40, 28, 23, 23, 20, 18, 16] // burndown data
      )}
    </div>
  );
};

export default BurnDownChart;
